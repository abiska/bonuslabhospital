/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hospital;

/**
 *
 * @author iuabd
 */
public class Doctor extends Person{
    
    //constructor
    public Doctor(String name) {
        super.name = name;
    }
    
    //getter
    public String getName() {
        return name;
    }
}
