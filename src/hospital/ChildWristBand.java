/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hospital;

/**
 *
 * @author iuabd
 */
public class ChildWristBand extends WristBand{
    
    //attribute(s)
    private String parentName;
    
    //getter(s)
    public String getParentName() {
        return parentName;
    }
    
    //constructor(s)
    public ChildWristBand(String barCode, String informationSection, String parentName) {    
        super(barCode, informationSection);
        this.parentName = parentName;
    }
}
