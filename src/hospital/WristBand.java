/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

package hospital;

/**
 *
 * @author iuabd
 */
public class WristBand {
    
    //attribute(s)
    protected String barCode;
    protected String informationSection;
       
    //constructor(s)
   public WristBand(String barCode, String informationSection) {
        this.barCode = barCode;
        this.informationSection = informationSection;
    }
    
    //getter(s)

    public String getBarCode() {
        return barCode;
    }

    public String getInformationSection() {
        return informationSection;
    } 
}
