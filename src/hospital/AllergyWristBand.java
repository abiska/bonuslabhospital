/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

package hospital;

/**
 *
 * @author iuabd
 */
public class AllergyWristBand extends WristBand{
    
    //attribute(s)
    private String allergy;
    
    //constructor(s)
    public AllergyWristBand(String barCode, String informationSection, String allergy) {    
        super(barCode, informationSection);
        this.allergy = allergy;
    }

    //getter(s)
    public String getAllergy() {
        return allergy;
    }
}
