/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */

package hospital;


/**
 *
 * @author iuabd
 */
public class HospitalSimulation {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       
       Doctor familyDoctor = new Doctor("James");
       Patient patient1 = new Patient("Sherlock","April 1, 2022", familyDoctor);
       WristBand wristband = new WristBand("ABC123", "ibuprofen");
       ChildWristBand childWristband = new ChildWristBand("DEF456", "ibuprofen", "Lola");
       AllergyWristBand allergyWristband = new AllergyWristBand("DEF456", "ibuprofen", "pollen");
       
       System.out.print("\n--------Info----------");
       System.out.println("\nPatient's name: "+ patient1.getName() + 
               "\nPatient's Date of Birth: " + patient1.getDateOfBirth() + 
               "\nPatient's Family Doctor: "+ familyDoctor.getName());
       
       System.out.print("\n--------Child----------");
       System.out.println("\nPatient's Barcode: "+ childWristband.getBarCode() + 
               "\nPatient's medication: "+ childWristband.getInformationSection() +
               "\nPatient's Parent: "+ childWristband.getParentName());
       
       System.out.print("\n--------Allergic----------");
       System.out.println("\nPatient's Barcode: "+ allergyWristband.getBarCode() + 
               "\nPatient's medication: "+ allergyWristband.getInformationSection() +
               "\nPatient's allergic to: "+ allergyWristband.getAllergy());
    }
}
