package hospital;

public class Patient extends Person {
    //attribute(s)
    private String dateOfBirth  ;
    private Doctor familyDoctor;
    
    //contructors
    public Patient(){}

    public Patient(String name, String dateOfBirth, Doctor familyDoctor) {
        super.name = name;
        this.dateOfBirth = dateOfBirth;
        this.familyDoctor = familyDoctor;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public Doctor getFamilyDoctor() {
        return familyDoctor;
    }

    public String getName() {
        return name;
    } 
}
